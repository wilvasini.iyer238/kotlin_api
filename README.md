# Kotlin_API
KotLin API Problem Statement 

Create an API using Springboot & kotlin to consume the API: https://restcountries.eu/rest/v2/all and parse response in a way that below two API should work:

Following endpoints should be exposed:

-          Get Method for all the countries

-          Get Method for all the countries of particular region, provided through parameter. [pass the region as param].

Values thats needed to be displayed are name, cioc, capital, currency name, language name

On deployment hit the following API URL :

Get Method for all the countries - http://localhost:8080/country

Get Method for all the countries of particular region, provided through parameter - http://localhost:8080/country/?region=Asia

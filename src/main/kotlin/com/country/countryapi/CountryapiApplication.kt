package com.country.countryapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CountryapiApplication

fun main(args: Array<String>) {
    runApplication<CountryapiApplication>(*args)
}


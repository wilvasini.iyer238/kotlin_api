package com.country.countryapi

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController

@RestController
class HomeController
{
    @Autowired
    lateinit var countryService: CountryService

    @RequestMapping("/country")
    @ResponseBody
    fun home(@RequestParam("region", required = false, defaultValue = "") region: String) : List<CountryModel>
    {
        var userName: List<CountryModel> = countryService.getCountries(region)

        return userName
    }
}
package com.country.countryapi

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject

@Component
class CountryService
{
    fun getCountries(region: String): List<CountryModel>
    {
        val restTemplate = RestTemplate()

        var uri:String = "https://restcountries.eu/rest/v2/all"

        var result:String = restTemplate.getForObject(uri, String())

        val gson = Gson()

        val arrayTutorialType = object : TypeToken<List<CountryModel>>() {}.type

        var countryModelData: List<CountryModel> = gson.fromJson(result, arrayTutorialType)

        if(region.trim() != "")
        {
            var filterdCountry: List<CountryModel> = countryModelData.filter { it.region == region }
            return filterdCountry
        }

        return countryModelData
    }
}

package com.country.countryapi

class CountryModel(
        val name: String,
        val capital:String,
        val languages: List<CountryLanguages>,
        val currencies: List<CountryCurrencies>,
        val cioc:String,
        val region:String
) {
    override fun toString(): String {
        return " [name: ${this.name},capital: ${this.capital},cioc: ${this.cioc},languages: ${this.languages},currencies: ${this.currencies},region : ${this.region}]"
    }
}

class CountryLanguages(
        val name:String
)
{
    override fun toString(): String {
        return "name: ${this.name}"
    }
}

class CountryCurrencies(
        val name:String
)
{
    override fun toString(): String {
        return "name: ${this.name}"
    }
}